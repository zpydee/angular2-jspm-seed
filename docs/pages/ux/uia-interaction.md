# Interacting with the app's UIA

## Background

The design of an appropriate user interface architecture is going to be critical to ensure that Audience will scale well from a user experience perspective as its scope of features expands. In addition, becuase the possibility exists that we might want to pursue other products down the line, it would nbe really useful to have a "UI Shell" that is reusable across a variety of applications.


## The Experience
Its important that our app feels unique to our users, and that it offers a user interface architecture and navigation paradigm that's not "just another app". Its also important that users instantly know where to look and click to find what they're looking for and that the interface makes our users confident.

Achieving the stated user experience goals requires that we bring together paradigms that users already understand with innovative UI elements.

The terminology we use for our navigation items are important. 

## User stories

Some of the user stories that are encapsulated within this experience include...

As a | I want to be able to | so I can | which we solve | in issue
---|---|---|---|---
Any user | navigate the app | find the functionality I need | now | ACM-30/31
Any user | select an Audience | choose my context of work | now | ACM-35
Admin User | white-label the app with my own logo | identify with the app | backlog | ACM-38

![Draft](../draft.jpg)

## Prototype
<div style="width: 960px; height: 720px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:960px; height:720px" src="https://www.lucidchart.com/documents/embeddedchart/dec9239a-f5f6-492b-84b0-ca0038ddc08d" id="5_ifRKISE8cc"></iframe></div>


## UI Components

Components | Overview
---|---
[top-nav-component](../features/uia/top-nav-component.md) | Primary navigation mechanism for the site. also contains site branding, buttons associated with side-box content and a button to access the user menu.
[context-menu-component](../features/uia/context-menu-component.md) | Used to select which Audience's entities are to be displayed in the content area and to switch between Audiences (contexts).
[user-menu-component](../features/uia/user-menu-component.md) | Contains links to all user profile and account management fucntionality.
[breadcrumb-component](../features/uia/breadcrumb-component.md) | used to provide visual feedback to the user about where they are in the site and importantly to navigate backwards anf forwards between list and detail views for entities.
[side-nav-component](../features/uia/side-nav-component.md) | Secondary, intra-task, site navigation. has expanded and collapsed modes.
[main-workspace-component](../features/uia/main-workspace-component.md) | The main part of the site where the user will complete most of their primnary tasks
[subtask-workspace-component](../features/uia/subtask-workspace-component.md) | Used for quick access to supporting tasks like chat, checking tasks and getting help without disrupting the flow of the primary task

## Services

None.

## Technical goals and considerations

- Across the app we will be using the Twitter Bootstrap UI framework (V4) for styling and UI components.
- Wherever possible, avoid toght coupling of AUdience specific features with the UI Shell. Doing so would limit our capability to make the components reusable in other apps.