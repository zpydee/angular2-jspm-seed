# Jira Issue Management

## Introduction
The Audience Experience Client project uses the [Jira Issue Management](https://www.atlassian.com/software/jira) solution from Atlassian as a mean to manage the work being done on the project.

As a member of our team, you should have a license for the tool and [access to this project in our Jira account](https://interactrdt.atlassian.net/browse/acm). If you cannot access our propject on Jira, please let us know.

The purpose if this section of the documentation is to give you a brief overview of how we expect you to use Jira and how you should use your commit messages to update Jira as you progress through your work. It is not intended to give you a deep understanding of Jira and all its features. If you do not have prior exposure to Jira and would like to learn more, [please visit the Jira help pages](https://confluence.atlassian.com/jirasoftwarecloud/jira-software-documentation-764477791.html).

## Our Jira Workflow

The diagram below illustrates the basic workflow that has been adopted by the Audience Experience Client team.

We have intentionally split the usual `In Progress` stage into 3 distinct stages to ensure that we adhere to good quality design principles and communication between team members at all times throughout the design, development and code review lifecycles. 

![Jira Workflow](jira-workflow.png)

Workflow Stage | Notes
---|---
To Do | Any issue that comes into our board will most likely start in the `To Do` column (unless it was part of a prior sprint and was not completed). When you're looking for new work to do, look in this column.
Design | The design stage of our workflow is intended for you to have all te relevant conversation with members of your team and users to collaboratively design the feature that you are working on. During the design stage, you will be expected to add / update documentation to this repository supporting your new feature or feature enhancement.
Develop / Test | This stage of any feature will most likely represent the largest portion of work (assuming you're a developer). However, with adequate documentation in place, your task will hopefully be somewhat simplified. You'll note that the stage is intentionally called `Develop / Test`. Our project adopts TDD principles and its our expectation that you will write test and app code in parallel.
Review | Before marking the feature as being completed, it is important to review the work you have done with a colleague or two. This normally involve making sure that your feature satisfies business users and alsl that you code satisfies our [coding standards](coding).
Done | Move the issue to this column when your feature is complete. At this stage, you would normally issue a `pull request` so that your feature can be integrated back into its parent branch.

## Feature Branches
The `master` branch of our repository has been configured not to accept pushes. Therefore, you will need to create a feature branch before starting work on any new feature.

Luckily, our instance of Jira is connected to our Bitbucket repository, and offers functionality that allows you to create feature branches within our agile board. To create a new branch, simply select an issue from the board and click the `Create branch` link in the side panel on the right. 

![Create Feature Branches](create-branch.png)

Doing this will give you the opportunity to create a new branch. **Do not change the branch name suggested by Jira.** Also, note how the branch name includes the issue identifier, in this case `ACM-8`. This will be important when you make commits to the repository.

![Name Feature Branches](name-new-branch.png)

In order to create a consistent way of working within the team, we have established some rules about how feature branches should be created in situations where they do or do not have parent stories. To understand this, please refer the diagram two above this paragraph.

Three circumstances occur:
1. In cases where an issue does not have a parent story (which most likely implies that its a simple story without a requirement for sub tasks), create a feature branch for the story (branching off `master`) and use this branch to commit your code.
2. In cases where a story has a number of sub-tasks, first create a branch for the story (off `master`) and use this branch for all sub-tasks within the story.
3. In circumstances wherein you're working on a parent story with child tasks and you need to assign a sub-task to someone else, create a child branch so that you can maintain your own code baseline separate to the other developer. 

## Committing to a feature branch

Once again, without going into detail ([which you can find here](https://confluence.atlassian.com/jirasoftwarecloud/processing-issues-with-smart-commits-788960027.html)), we have activated `smart commits` for our Jira project. As a result, when making commits, please use one of the following format:

- To commit to a feature branch without transitioning to a new stage in the Jira workflow:

```
// git commit -am "<JIRA_ID> #time <time in w/d/h/m> <commit message here>"
// eg:

git commit -am "ACM-8 #comment #time 2h 30m Made some changes"

```

- To commit to a feature branch and tranition to a new stage in the Jira workflow at the same time:

```
/ git commit -am "<JIRA_ID> #transition #time <time in w/d/h/m> <commit message here>"
// eg:

git commit -am "#design ACM-8 #time 2h 30m Made some changes"
```

The transition names supported in commits are `design`, `dev`, `review` and `done`.

## Jira Add-Ons
For your benefit, you'll find a couple of Jira add-ons in our account. You can access more of them from the "more" option when looking at any issue...

- **LucidCharts:** Use it for creating interactive prototypes and any other diagrams you need to get your ideas across. LucidCharts allows you to embed these mockups and diagrams directly into your documentation in an interactive format with `html`. If you prefer, you can also export the diagrams as images in upload them to our docs directory.
