![Draft](../../draft.jpg)

# Feature Specification Template

When writing feature specifications, please use the `markdown` below as a starting point for your documentation.

> If your feature consists of a number of components and you feel it makes sense to repeat this template for each sub-componet, please feel free to use this approach. Doing so will place a link to each component in the `mkdocs` side navigation.


## Feature Spec: < FEATURE NAME >
### Overview

** Target Release Version:** < VERSION NUMBER >

** Document Status:** < DRAFT / FINAL >

** Document Owner:** < YOUR NAME (YOUR EMAIL ADDRESS) >

** Other Contributors:**
- < LIST OTHER CONTRIBUTOR NAMES >

** Primary Developer:** <  YOUR / OTHER NAME >

** Related Componenets / Services:**
- <  LIST COMPONENT / SERVICE LOCATIONS:
- e.g. /app/home/home-component.ts
- e.g. /common/login-service.ts >

** Should this feature be reusable outside of Audience:** <  YES / NO / NOT SURE YET >

### Background
< PROVIDE ANY BACKGROUND INFORMATION THAT YOU FEEL MIGHT BE IMPORTANT >

### User Goals
 < LIST THE USER GOALS THAT YOUR FEATURE IS ATTEMPTING TO SATISFY IN PARAGRPAH OR BULLET POINT FORMAT AS YOU SEE FIT.

### Technical Goals
 < LIST ANY TECHNICAL GOALS THAT YOU BELIEVE THE DEVELOPER SHOULD AIM FOR IN BUILDING YOUR FEATURE IN PARAGRPAH OR BULLET POINT FORMAT AS YOU SEE FIT.
 
 ### Key Assumptions
< PROVIDE ANY ASSUMPTIONS YOU"VE MADE THAT YOU FEEL MIGHT BE IMPORTANT >

### Related Componenets / Services

< PLACE COMPONENT / SERVICE ARHITECTURE DIAGRAM AND ANY SUPPORTING NOTES HERE >

### User Interface & User Requirements

< PROVIDE WIREFRAMES AND REQUIREMENTS SPECIFICATIONS TO AID IN UNDERSTANDING THE PROPOSED DESIGN OF THE FEATURE >

### Test & Acceptance Criteria
< LIST THE ACCEPTANCE CRITERIA FOR THIS FEATURE IN A WAY THAT THEY CAN SERVE AS THE BASIS FOR WRITING TESTS >

### Exclusions & Future Plans
< LIST ANYTHING THAT HAS SPECIFICALLY BEEN EXLUCED FROM YOUR FEATURE DESIGN AND PROPOSED FUTURE ENHANCEMENTS >
  
