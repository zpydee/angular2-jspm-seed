# Introduction

## Important Note - ReadMe
If you havent already, please [read the project orienttation guide on Bitbucket](https://bitbucket.org/axperience/a2-client-main) before continuing with this documentation.

## Purpose of this Documentation
The goal of this documentation is to serve primarily as a design palette for creating new features and a reference point for enhancements. It is the expectation of the project that when you commence working on any feature, that you will start by creating documentation fo the piece of work you are undertaking before you commence development and that you will make sure the documentation for the work you complete is kept up to date.

Having adequate documentation ensures that:

- Before you start writing code, you have documented your thoughts regarding the design. A well organised document will (hopefully) result in better organised code.
- Writing documentation, especially when expressed from a user perspective, will give you a great idea of what you code actually needs to to, and this serves as a fantastic precedent to writing your tests.
- If, in the future, you or anyone else in the team has to write enhancements to your code or your feature, your documentation will help bring them up to speed on your design approach very quickly. 
- Business users, who do not have the same capability to read code as developers, will have a place to come to understand the way our application works, and that they will will be able to consume user-friendly content.

When you approach writing documentation, please try to make sure it has tge becessary characteristics to achieve these goals.

## Structure of this Documentation

All documentation has been added to this repository under one of the following four major sections (accessible via the top menu)

Section | Contains
---|---
Home | This section. It serves as an introduction to the rest of the documentation and is intended for first time readers.
Developer Guide | Content relating to the project that will be of interest to developers that are starting to work on the project or who need to be involved depoloyment of the project 
User Experience | Contains outlines of all the user stories that the Audience Experience client aims to support. THe portion of the document is arranged by user tasks and may or may not span more that just one feature.
Features | Outlines the functional and technical design considerations for each of the components with the Audience Experience application. There should be a one-to-one relationship between a feature document and a component or set of related components in the `/app` directory. 

## What Next?
If you haven't aleady, please take a look at the Developer Guide section of our documentation, starting with the [Jira Issue Management](guide/jira) section, and thereafter the [Coding Standards](guide/coding) section.