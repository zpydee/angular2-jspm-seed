# top-nav-component

## Main Component

### Jira Isssue

[ACM-2](https://interactrdt.atlassian.net/browse/ACM-2) / ACM-30

### UX Specification

[UIA Interaction](../../ux/uia-interaction.md)

### Description

![top-nav-component](top-nav-component.png)

`top-nav-component` is a ***reusable*** component, and has three features:

#### Site Logo

The site logo is simply the branding image that's displayed on the top left of the navbar. However, it does include some functionality:

- If an incorrectly sized image is passed to it, it will resize it appropriately, and adjust the layout of the navbar to accomodate wider images if required.

#### Main Navigation Items

> Note: When I talk about the main navigation items, I'm also referring to the dropdown menu that might appear for some of the items. They're thus the same feature.

The navigation items are the primary mechanism for accessing functionality within the app. The items displayed are passed by the components container as a simple `json` object, and present three outcomes when clicked:

- If the navigation item has sub-items, it expands to display those items in a drop-down menu (currently only one level of dropdown is supported).
- If the item does not have any sub-iems,
    - Clicking the navigation item results in the user being navigated to a route attached to the navigation item, or
    - Clicking the navigation items results in a function in the container being called.  

#### Action Buttons

The navbar's action buttons operate similarly to the navigation items in that they can either display a sub-item, navigate to a route or call a container function, but the have some key enhancements associated with them:

- The action button is represented by an icon,
- Some action items may need notification counters
- The sub-items to an action item may be a dropdown menu, but it could also be a complete component of its own.


> Note: It is possible given the fact that acton items could just be icons with specific functionality passed to them, that this may be converted into its own component down the line. We'll see how the app unfolds...

### Specs

1. Site Logo:
    1. When the containing page loads,
        1. it is populated with an image passed to it by its container
        1. will shrink an over-sized image to the correct dimensions (set height)
        1. if the image is not square, it will adjusts its alloted size and move the nav items over
1. Main Navigation Items:
    1. When the containing page loads,
        1. it is populated with items that are passed in by its container 
        1. items with sub-items will get a down caret
    1. When a user click on a navigation item,
        1. if the item has sub items, a dropdown with the sub-items will be displayed
        1. 

![Draft](../../draft.jpg)

## Sub-component: NONE
> This component does not have any sub-components  

