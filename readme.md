# Audience Experience (axperience) Client

## Overview
The Audience Experience solution is a web application offers companies a range of tools to help them in measuring, managing and optimising the experience that they deliver to their customers. This [short video](https://vimeo.com/164812065) will help you to understand the product's positioning.

When it is complete, Audience will bring together a range of features like data visulation, agile task management, social plugins. For more information on the feature roadmap that is being proposed, please take a look at the `mkdocs` documentation available within this repository.

The Audience Experience Client (`a2-client-main`) employs the following technology:

- `systemjs` for `es2015` module loading
- `jspm` as the front-end package manager
- `angular 2` as a UI framework, written in `typescript`.
- `karma` as a test runner
- `wallabyjs` for an integrated testing experience (if you have this installed in your IDE)

---

---

## License
This software is closed-source and may not be copied or duplicated in any way other than for the explicit purposes of working on the project and furthering its development. By pushing code into our repository, you automatically cede all copyright in your code to us. Please do not clone this project if you do not accept these terms of engagement.

---

---

## Getting Started

### Software Requirements

> **NOTE:** This section of the document is intended primarily for non-technical team members to help them in getting up and running. However, if you run into any problems running this app or accessing our documentation, please refer here for support.

Any web application consists of two main components, a `client` and a `server`. The client is everything that you see when you load the website into your browser. This project repository contains all the code that is needed to make that happen. The server the software that is responsible for sending the client software up to your browser, and it also performs inmportant tasks like making sure you are an authenticated user when you try to access our application and talking to the back-end database. Our `server` solution is locaged in another project repository - we chose to separate our `client` and `server` repositories for a number of reasons.

Because our `server` is located in another project repository, we have included a `dummy server` in this repository that you will use to serve the client up to your browser. In order to run this dummy server on your computer (and in fact even to run our main server later), you will need to install `nodejs` on your computer. `nodejs` is a modern web application server technology that allows us to run javascript code directly on our computers. It also include a tool, called `npm` which we will be using in our project to install all the supporting software (packages) we need and to run and test our project.

---

To download and install `nodejs`:

1. Visit the `nodejs` website to [choose a version to download](https://nodejs.org/en/download/current/), or...
    - Install version 6.0.0 [for Mac OSx](https://nodejs.org/dist/v6.0.0/node-v6.0.0.pkg)
    - Install version 6.0.0 [for Windows](https://nodejs.org/dist/v6.0.0/node-v6.0.0-x64.msi) (64-bit)
    
2. Once your download completes, install it and follow the on-screen instructions.

3. After installing open the terminal on Mac OSx or the Command Prompt on Windows. All instructions you'll issue to both `nodejs` and `npm will be done via the terminal
> NOTE: All the instructions below are for a Mac OSx terminal, but they should work equally well on a Windows machine.

4. Next, check that `nodejs` and `npm` installed correctly. if you get version numbers back for each, they're installed and working. If not, [please email me](mailto:devsupport@axperience.co).
> NOTE: You can recognise commands you type into the terminal below by the `$` thst precedes them. If there is no `$`, this means its the response you'll get back from terminal after hitting `ENTER`. 

```
$ node --version
v6.0.0

$ npm --version
3.6.0

```
---

The next thing you're going to need to set up on your computer is `git`, which is a version control system. You will be using `git` to clone this repository onto your computer to your computer's version of the repository to the latest version when you want to. By installing `git`, you will be able to have the very latest version of Audience on your machine whenever you want, with just a few simple keystrokes.

To get `git` up and running:

1. Download `git`
    - for [Mac OSx](https://git-scm.com/download/mac)
    - for [Windows](https://git-scm.com/download/win)
    
2. Once your download completes, install it and follow the on-screen instructions.
> NOTE: On Macs, you may get a warning that the installation has been blocked because its downloaded from the Internet. To get around this, go to `System Preferences > Security & Privacy` and at the bottom of the `General` tab you'l find an option to install `git` anyway.

3. Open your terminal to check that `git` was installed.
> NOTE: Your version of `git` may be more recent, depending on when you're reading this, but that's okay.

```
$ git --version
git version 2.8.1

```
---

In order to work with our git repository, you are going to need to generate a secure shell "key", an `ssh public key`. `ssh` most likely came installed with your operating system, but if not, you'll need to install it.

To get up and running:

1. use `ssh-keygen` to generate an `ssh public key`. When asked for a file location, just hit `ENTER` to accept the default. Do the same when asked for a passphrase.

2. After generating a key, check that you have `id_rsa` and `id_rsa.pub` files.

3.  Type `cat ~/.ssh/id_rsa.pub` to get your public key, then copy it and mail it to me so I can add you to our list of approved `ssh public keys`.

```
$ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/emmap1/.ssh/id_rsa):
Created directory '/Users/emmap1/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Users/emmap1/.ssh/id_rsa.
Your public key has been saved in /Users/emmap1/.ssh/id_rsa.pub.
The key fingerprint is:
4c:80:61:2c:00:3f:9d:dc:08:41:2e:c0:cf:b9:17:69 you@yourcomputer.local
The key's randomart image is:
+--[ RSA 2048]----+
|*o+ooo.          |
|.+.=o+ .         |
|. *.* o .        |
| . = E o         |
|    o . S        |
|   . .           |
|     .           |
|                 |
|                 |
+-----------------+

$ ls -a ~/.ssh 
.        ..        id_rsa        id_rsa.pub    known_hosts

$ cat ~/.ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDhfwsHIduAGJ27zwTHsR9mtBgX
+oCUhCbC0ZejIw0M84Rz1idTy7eumhsp0+NX+dACKMlZrfGNMwr8c3m9BeiSJa/
6OzFifVPBrHFU84SY2Zi3mXhHJ4bP5x6LDUhHu5mVFGv3Gz7povskaDUxk/LtkD
6v2MUy0N+aJ1jHdxaUema5mV9BOGIYEDbqkY7kwx7DweONNsd54zt2s4L9fMXwp
JLN80/CzsYzym7esNtW/XBBfIOp7gWzJ9GO1F+zsnS439N0tH5e4ffe56V/xXlcw
PQpAd7Cl5UI6z7PxKFJWSDEhpMRXeYoDrODIa4NuJw5GrjdijY5Aj2uaPCw5rX53
cT/ you@yourcomputer.local
```

---

Lastly, in order to be able read our documentation, you will need to install `python`, `pip`, and `mkdocs`.

To get these up and running:

1. Download `Python` and `pip`:
    - for [Mac OSx](https://www.python.org/ftp/python/2.7.11/python-2.7.11-macosx10.6.pkg)
    - for [Windows](https://www.python.org/ftp/python/2.7.11/python-2.7.11.amd64.msi)
    
2. Once your download completes, install it and follow the on-screen instructions.

3. Open your terminal to check that `python` and `pip` were installed.
> NOTES:
> - Your version of `python` may be more recent, depending on when you're reading this, but that's okay.
> - The version of 'python' you're installing will come with `pip` included.

```
$ python --version
Python 2.7.11

$ pip --version
pip 8.1.1 from /Library/Python/2.7/site-packages (python 2.7)
```

4. Use `python` to install `mkdocs`, and then check that it's been installed.

```
$ pip install mkdocs
...

$mkdocs --version
mkdocs, version 0.15.3
```

**Okay, you're all set!**

---

---
 
### Cloning the repository
Once you've got all the required software up and running, all you'll need to do is to clone this project repository onto your computer in order to run the web application in your browser. You can clone the repo into whichever directory on your computer that you wish. I'm going to assume you'll be installing it in the root of your User folder.

```
$ cd ~

$ git clone git@bitbucket.org:axperience/a2-client-main.git
Cloning into 'a2-client-main'...
remote: Counting objects: 257, done.
remote: Compressing objects: 100% (165/165), done.
remote: Total 257 (delta 134), reused 169 (delta 82)
Receiving objects: 100% (257/257), 35.52 KiB | 0 bytes/s, done.
Resolving deltas: 100% (134/134), done.
Checking connectivity... done.
```

---

---

### Global npm packages

This project, and in particular the `npm` scripts it runs, require some dependencies to be run from the command line. As a result, you need to install some packages locally, including:
 - `karma-cli` which is used for running unit-test whilst you code,
 - `jspm` which is used as the javascript package manager for install client dependencies,
 - `typings` to download all `.d.ts` files for `typescript` types.
 
```
$ sudo npm install -g karma-cli jspm typings
```
 

 > Because you'll probably need sudo to install globally, you'll be prompted for a password. Use your operating system password.

 
 ---
 
 ---

### Running the app

The repo you will just have cloned incorporates `npm run` scripts for pretty much everything you'll need to do. Therefore, to run the application:

1. Use `npm install` to download and install all project dependencies.
> **NOTE:** the install script includes postinstalls that will also download and install all `jspm` and `typings` dependencies.

2. Use `npm start` to run `lite-server`, which will automatically launch a browser window for you.
> **NOTE:** `lite-server` is based on `browser-sync` and it has been configured to automatically refresh whenever you change any `.ts`, `js`, `.html`, `htm`, or `.css` files located in the `/app` fiolder. The site is served from `localhost:3000`.

```
$ cd a2-client-main

$ npm install
...

$ npm start
...

```
---

---

### Running Tests
This project is set up to run tests both within `karma` and within `wallabyjs` (if you use this tool.

To run `karma` tests:
1. use npm scripts to run tests.
> **NOTES:**
> - `karma.conf.js` is configured to watch files and rerun on any changes in either app or test files.
> - Both the `progress` and `spec` reporters have been installed. YOu can change to the spec reporter in `karma.conf.js`

```
$ npm test
Chrome 50.0.2661 (Mac OS X 10.11.4): Executed 1 of 1 SUCCESS (0.005 secs / 0.001 secs)
```
---

---

### Updating your repository
For your convenience, we have included a convenience `npm` script to update the project.

```
$ npm run update
...

$ npm start
...
```

---

---

## Documentation
All documentation for the project is written in `markdown` and is served via the browser using `mkdocs`. It is a core culture of our team that documentation should always be totally complete and that it must act as a support for developers to ensure best practise design patterns are adhered to in our code-base.

It is therefore an expectation of ours that you will take the time to read any relevant reference articles before adding a new component to our solution. Your lack of effort in this area may result in code being rejected in code-reviews.

### Serving documentation

We have included a convenience `npm` script for serving documentation. Once built and served, you can access the documentation as `localhost:8000`.

```
$ npm run docs

INFO    -  Cleaning site directory 
INFO    -  Building documentation to directory: /Users/zpydee/hack/a2-client-main/docs/site 
INFO    -  Building documentation... 
INFO    -  Cleaning site directory 
[I 160430 17:03:12 server:281] Serving on http://127.0.0.1:8000
[I 160430 17:03:12 handlers:59] Start watching changes
[I 160430 17:03:12 handlers:61] Start detecting changes
```

### Conrtibuting to documentation
Because adequate documentation is so important to us, you wll be expected to contribute to our repository of information for the code you write, and for the feature additions and enhancements it supports.
> No documentation = no code review.

Our documentation source is located in `/docs/pages` and any assets (images etc.) you need to access are located (or should be added to) `/docs/pages/assets`. If you need to add new pages to the document navigation, you'l need to update `mkdocs.yml`.

In adding documentation, please adhere to the folowing rules:
- Only good English will be accepted. Please focus on acceptable spelling and grammar usage.
- Always explain features from the perspective of a user story. Documentation should be able to be understood by non-technical, business users. 
- Be thorough in your documentation. Please don't assume that others reading your documentation have your prior knowledge. You documentation must be able to stand alone. Include diagrams, screenshots and examples whenever possible to enhance your reader's user experience.
- Use links. If you are referring to other content that's important in the context of your documentation, link to it. Don't expect your reader to find important stuff by themselves.

---

---


## Contributing to the Codebase
If you are a developer that will be contributing to our codebase, or if you are assisting with writing documentation, you should know that the `master` branch of our project will not accept pushes. Therefore, please create a feature branch before working on your contribution and push to your new branch.

Your code will be incporporated back into the `master` branch after code-review. 

---

---

## Tests
As with documenttion, tests are critically important to the success of our project, and nothing less that complete code coverage is considered acceptable (except in circumstances where there's a good reason). The major focus in the project is on solid unit testing.
> <95% code coverage = no code review

When writing tests:
- your test files should be placed in the same directory, and alongside, as the code that's being tested. This makes it easy to spot what files aren't yet tested.
- One test file per code file. One code file per test file.
- test files should have the same name as the file being tested, and `_spec.ts` added as an extension

---

---

## JIRA Issue Management
The Audience Experience client project uses `JIRA Software` as our issue management platform. If you don't already have access to `JIRA`, please email us and we'll set you up.

Please use `JIRA` as your primary interface for branching in order that your features are linked to issues.

Please also ensure that you refer to the `JIRA` issue under development with each of your commits in order that we can track issues properly.
> Inadequate `JIRA` interaction = no code-review

---

---