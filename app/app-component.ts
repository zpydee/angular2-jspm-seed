// import $ from 'github:components/jquery@2.1.4';
// import bootstrap from 'bootstrap4';
// import 'twbs/bootstrap/css/bootstrap.css!';

import {Component} from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-beta';

import {HomeComponent} from './home/home-component';
import {WidgetsComponent} from './widgets/widgets-component';

@Component({
    selector: 'ax-app',
    templateUrl: 'app/app-component.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ROUTER_PROVIDERS]
})
@RouteConfig([
  {
    path: '/home',
    name: 'Home',
    component: HomeComponent,
    useAsDefault: true
  }, {
    path: '/widgets/...',
    name: 'Widgets',
    component: WidgetsComponent
  },
])
export class AppComponent {
    public appTitle: string = 'Welcome to Ax';
}
